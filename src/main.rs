extern crate clap;

use clap::{Arg, App};
use std::fs::File;
use std::io::{BufRead, BufReader};

pub mod gen;
pub mod cfg;

fn main() {
    let matches = App::new("AA2F Generator")
        .about("Attemtps to create longest AA2F word")
        .arg(Arg::with_name("thread_num")
            .short("t")
            .long("threads")
            .help("Number of worker threads to be spawned")
            .default_value("4")
            .takes_value(true))
        .arg(Arg::with_name("input_path")
            .short("i")
            .long("input")
            .help("(Optional) Path to the file containing word(s) that should be extended")
            .takes_value(true))
        .arg(Arg::with_name("backtrack")
            .short("b")
            .long("backtrack")
            .help("Whether or not word(s) in the input file should be backtracked")
            .requires("input_path"))
        .get_matches();

    let mut initial_words: Vec<String> = Vec::new();
    let thread_num: usize = matches.value_of("thread_num").expect("Could not ge thread number")
        .parse::<usize>().expect("Thread number should be a positive number");
    if let Some(input_path) = matches.value_of("input_path") {
        let file = File::open(input_path).expect("Could not open the specified input file");
        let reader = BufReader::new(&file);
        for line in reader.lines() {
            initial_words.push(line.expect("Something is very wrong with the input file"));
        }
    } else {
        initial_words.push("a".to_string());
    }
    let should_backtrack: bool = matches.is_present("backtrack");
    if should_backtrack {
        let mut bt_groups: Vec<Vec<gen::Word>> = Vec::new();
        for wrd in initial_words.iter() {
            bt_groups.push(gen::generate_backtrack(&gen::string_to_word(wrd)));
        }
        for group in bt_groups {
            for word in group.iter() {
                initial_words.push(gen::word_to_string(word));
            }
        }
    }
    initial_words.sort_unstable_by(|a, b| a.len().cmp(&b.len()));

    while let Some(longest) = initial_words.pop() {
        println!("Starting gen on prefix of length {}", longest.len());
        //let mut my_gen = gen::Generator::new(&vec![longest], should_backtrack, thread_num);
        let mut my_gen = gen::Generator::new(&vec![longest], false, thread_num);
        my_gen.generate();
    }

}
