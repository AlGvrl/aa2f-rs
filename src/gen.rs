use std::collections::binary_heap::BinaryHeap;
use std::sync::{Mutex, Arc};
use std::sync::atomic;
use std::thread;
use std::ops::{Deref, DerefMut};
use std::cmp::{PartialEq, Eq, PartialOrd, Ord, Ordering};
use cfg;

///An alias for a `Vec<u8>`, represents a word.
pub type Word = Vec<u8>;

///A wrapper for a Word, which implements Cmp based on a given heuristic.
///Internally caches the result of [priority-calculating function](fn.calculate_comparison_key.html).
///Wrapper can be automatically dereferenced to the wrapped `Word` under some circumstances.
#[derive(Debug)]
pub struct WordWrapper {
    data: Word,
    comparison_key: i64,
}

///Represents the action that a thread should execute.
#[derive(Debug)]
pub enum Task {
    ///Spawn word's children and push those of them that are AA2F back to the queue.
    ProcessWord(WordWrapper),
    ///Kill the thread.
    Halt
}

impl Deref for WordWrapper {
    type Target = Word;

    fn deref(&self) -> &Word {
        &self.data
    }
}

impl DerefMut for WordWrapper {
    fn deref_mut(&mut self) -> &mut Word {
        &mut self.data
    }
}

impl PartialEq for WordWrapper {
    fn eq(&self, other: &Self) -> bool {
        self.comparison_key == other.comparison_key
    }
}

impl Eq for WordWrapper {

}

impl PartialOrd for WordWrapper {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.comparison_key.partial_cmp(&other.comparison_key)
    }
}

impl Ord for WordWrapper {
    fn cmp(&self, other: &Self) -> Ordering {
        self.comparison_key.cmp(&other.comparison_key)
    }
}

impl WordWrapper {
    ///Construct a new Wrapper from a given Word. Consumes the Word.
    ///#Examples
    ///```
    ///let ww = WordWrapper::new(vec![0u8, 1, 0]);
    ///```
    pub fn new(word: Word) -> WordWrapper {
        WordWrapper{
            comparison_key: calculate_comparison_key(&word),
            data: word,
        }
    }

    ///Creates and returns all possible children of the Wrapped Word.
    ///#Examples
    ///```
    ///let ww = WordWrapper::new(vec![0u8, 1, 0]);
    ///let expected_children = vec![
    ///    WordWrapper::new(vec![0u8, 1, 0, 0]),
    ///    WordWrapper::new(vec![0u8, 1, 0, 1]),
    ///    WordWrapper::new(vec![0u8, 1, 0, 2])
    ///];
    ///let children = ww.spawn_children();
    ///assert_eq!(children, expected_children);
    ///```
    pub fn spawn_children(&self) -> Vec<WordWrapper> {
        let mut result = Vec::with_capacity(cfg::CARDINALITY);
        for i in 0..cfg::CARDINALITY {
            let mut new_word: Word = Vec::with_capacity(self.data.len() + 1);
            new_word.clone_from(&self.data);
            new_word.push(i as u8);
            result.push(WordWrapper::new(new_word));
        }
        result
    }
}

///Function that calculates the value, by which word's priority will be determined.
///The higher the returned value, the more the word will be prioritized.
///Redefine if needed.
pub fn calculate_comparison_key(word: &Word) -> i64 {
    let length = word.len() as i64;
    /*let mean: i64 = length / (cfg::CARDINALITY as i64);
    let mut unbalancedness = 0i64;
    unbalancedness -= length % (cfg::CARDINALITY as i64);
    let parikh = calculate_parikh_vector(word);
    for i in parikh.iter() {
        unbalancedness += (*i as i64 - mean).abs() as i64;
    }

    length * length - unbalancedness * unbalancedness * unbalancedness * 27 / 8 / length*/
    length
}

impl PartialEq for Task {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (&Task::ProcessWord(ref word_1), &Task::ProcessWord(ref word_2)) => {
                word_1 == word_2
            },
            (&Task::Halt, &Task::Halt) => true,
            _ => false
        }
    }
}

impl Eq for Task {

}

impl PartialOrd for Task {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (&Task::ProcessWord(ref w_1), &Task::ProcessWord(ref w_2)) => w_1.partial_cmp(&w_2),
            (&Task::Halt, &Task::Halt) => Some(Ordering::Equal),
            (&Task::Halt, _) => Some(Ordering::Greater),
            (_, &Task::Halt) => Some(Ordering::Less),
            //_ => panic!()
        }
    }
}

impl Ord for Task {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (&Task::ProcessWord(ref w_1), &Task::ProcessWord(ref w_2)) => w_1.cmp(&w_2),
            (&Task::Halt, &Task::Halt) => Ordering::Equal,
            (&Task::Halt, _) => Ordering::Greater,
            (_, &Task::Halt) => Ordering::Less,
            //_ => panic!()
        }

    }
}

///The "main" structure.
///Acts as a thread coordinator and queue storage.
#[derive(Debug)]
pub struct Generator {
    task_queue: Arc<Mutex<BinaryHeap<Task>>>,
    max_len: Arc<atomic::AtomicUsize>,
    thread_number: usize,
}

impl Generator {
    ///Create a new generator.
    ///
    ///Receives 3 parameters:
    ///
    ///- `Word`(s) that should be extended
    ///
    ///- whether or not those words should be backtracked
    ///
    ///- number of worker threads that should be spawned by generator.
    ///
    ///Does not check if given `Word`(s) are AA2.
    ///This should be done manually before creating a generator, if needed.
    ///
    ///#Examples
    ///```
    ///let words = vec!["abc".to_owned(), "aabb".to_owned(), "abba".to_owned()];
    ///let aa2f_words: Vec<String> = words.into_iter()
    ///    .filter(|x| is_aa2f_full_check(&string_to_word(x))).collect();
    ///assert_eq!(aa2f_words, vec!["abc", "aabb"]);
    ///let mut gen = Generator::new(&aa2f_words, false, 1);
    ///```
    pub fn new(strings: &[String], backtrack_input: bool, threads: usize) -> Generator {
        let max_len: Arc<atomic::AtomicUsize> = Arc::new(atomic::ATOMIC_USIZE_INIT);
        let queue: Arc<Mutex<BinaryHeap<Task>>> = Arc::new(Mutex::new(BinaryHeap::with_capacity(cfg::HEAP_CAPACITY)));
        {
            let mut queue_lock = queue.lock().expect(&*format!("Failed at line {}", line!()));
            for s in strings {
                let word = string_to_word(s);
                if backtrack_input {
                    let bt_words = generate_backtrack(&word);
                    for bt_word in bt_words.into_iter() {
                        queue_lock.push(Task::ProcessWord(WordWrapper::new(bt_word)));
                    }
                } else {
                    queue_lock.push(Task::ProcessWord(WordWrapper::new(word)));
                }
            }
        }
        Generator{
            max_len: max_len,
            task_queue: queue,
            thread_number: threads,
        }
    }

    ///Starts endless generation.
    pub fn generate(&mut self) {
        self.generate_until(|_: &WordWrapper| false);
    }

    //will run infinitely if/when queue gets empty (?) V

    ///Starts generation.
    ///
    ///Receives one parameter -- closure function which judges if generation should continue.
    ///The process is stopped when some word makes the closure return `true`.
    ///
    ///Returns `String` representing a `Word` which caused the halting of generator.
    ///
    ///#Examples
    ///```
    ///let mut gen = Generator::new(&vec!["a".to_owned()], false, 1);
    ///let result = gen.generate_until(|x| x.len() > 4);
    ///assert_eq!(result.len(), 5);
    ///```
    pub fn generate_until<F>(&mut self, should_halt: F) -> String
    where F: 'static + Send + Sync + Fn(&WordWrapper) -> bool {
        let mut threads = Vec::with_capacity(self.thread_number);
        let thread_number = self.thread_number;
        let arced_should_halt = Arc::new(should_halt);
        for _ in 0..thread_number {
            let mut missed_pops = 0i32;
            let queue = self.task_queue.clone();
            let max_len = self.max_len.clone();
            let should_halt = arced_should_halt.clone();
            let new_thread = thread::spawn(move || {
                loop {
                    let current_word: WordWrapper;
                    {
                        let mut queue_lock = queue.lock().expect(&*format!("Failed at line {}", line!()));
                        match queue_lock.pop() {
                            Some(task) => {
                                missed_pops = 0;
                                match task {
                                    Task::ProcessWord(word) => current_word = word,
                                    Task::Halt => return None,
                                }
                            }
                            None => {
                                if missed_pops > 1_000_000 {
                                    return None;
                                } else {
                                    missed_pops += 1;
                                }
                                //thread::sleep_ms(5);
                                continue;
                            }
                        }
                    }
                    let children: Vec<WordWrapper> = current_word.spawn_children()
                    .into_iter()
                    .filter(|wrd| is_aa2f_partial_check(wrd))
                    .collect();
                    //Comment out from here...
                    for child in children.iter() {
                        if child.len() > max_len.load(atomic::Ordering::Relaxed) {
                            max_len.store(child.len(), atomic::Ordering::Relaxed);
                            println!("{} {}",child.len(), word_to_string(child));
                        }
                    }
                    //...to here to avoid printing words
                    let mut queue_lock = queue.lock().expect(&*format!("Failed at line {}", line!()));
                    for child in children.iter() {
                        if should_halt(child) {
                            for _ in 1..thread_number {
                                queue_lock.push(Task::Halt);
                            }
                            return Some(word_to_string(child));
                        }
                    }
                    /*if children.iter().any(|x| should_halt(x)) {
                        for _ in 0..thread_number {
                            queue_lock.push(Task::Halt);
                        }
                    }*/
                    for child in children.into_iter() {
                        queue_lock.push(Task::ProcessWord(child));
                    }
                }
            });
            threads.push(new_thread);
        }
        let mut result: Option<String> = None;
        for thread in threads {
            if let Some(res) = thread.join().expect("Could not join thread") {
                result = Some(res);
            }
        }
        result.unwrap_or("".to_owned())
    }

}

///Returns cumulative parikh vector of the word.
fn calculate_parikh_chain(word: &Word) -> Vec<[usize; cfg::CARDINALITY]> {
    let mut result = Vec::with_capacity(word.len() + 1);
    result.push([0; cfg::CARDINALITY]);
    for i in 0..word.len() {
        let mut new_elem = result[i].clone();
        new_elem[word[i] as usize] += 1;
        result.push(new_elem);
    }
    result
}

///Returns parikh vector of the word.
///#Examples
///```
///assert_eq!(calculate_parikh_vector(&vec![0u8, 1, 0, 2, 1]), [2, 2, 1]);
///assert_eq!(calculate_parikh_vector(&vec![]), [0,0,0]);
///```
pub fn calculate_parikh_vector(word: &Word) -> [usize; cfg::CARDINALITY] {
    let mut result = [0; cfg::CARDINALITY];
    for letter in word.iter() {
        result[*letter as usize] += 1;
    }
    result
}

///Returns difference of two parikh vectors.
fn parikh_diff(p1: &[usize; cfg::CARDINALITY], p2: &[usize; cfg::CARDINALITY]) -> [usize; cfg::CARDINALITY] {
    let mut result = [0; cfg::CARDINALITY];
    for i in 0..p1.len() {
        result[i] = p2[i] - p1[i];
    }
    result
}

///Checks if any of the word's factors that include the last letter is Almost-Abelian-Square.
///#Examples
///```
///assert_eq!(is_aa2f_partial_check(&vec![0u8, 1, 0, 2, 0, 1, 0]), true);
///assert_eq!(is_aa2f_partial_check(&vec![0u8, 1, 0, 1]), false);
///assert_eq!(is_aa2f_partial_check(&vec![0u8, 1, 0, 1, 2]), true);
///```
pub fn is_aa2f_partial_check(word: &Word) -> bool {
    if word.len() < 4 {
        return true;
    }
    let l = word.len();
    let parikh_chain = calculate_parikh_chain(word);
    for i in 2..(l / 2 + 1) {
        let diff2 = parikh_diff(&parikh_chain[l - i], &parikh_chain[l]);
        let diff1 = parikh_diff(&parikh_chain[l - 2 * i], &parikh_chain[l - i]);
        if diff1 == diff2 {
            return false;
        }
    }
    true
}

///Check if any of the word's factors is Almost-Abelian-Square.
///#Examples
///```
///assert_eq!(is_aa2f_full_check(&vec![0u8, 1, 0, 2, 0, 1, 0]), true);
///assert_eq!(is_aa2f_full_check(&vec![0u8, 1, 0, 1]), false);
///assert_eq!(is_aa2f_full_check(&vec![0u8, 1, 0, 1, 2]), false);
///```
pub fn is_aa2f_full_check(word: &Word) -> bool {
    let l = word.len();
    for part_len in 1..(l + 1) {
        for window in word.windows(part_len) {
            if !is_aa2f_partial_check(&window.to_vec()) {
                return false;
            }
        }
    }
    true
}

///Converts `Word` to "abc"-string.
///#Examples
///```
///assert_eq!(word_to_string(&vec![0u8, 1, 2]), "abc".to_owned());
///assert_eq!(word_to_string(&vec![]), "".to_owned());
///```
pub fn word_to_string(word: &Word) -> String {
    let utf: Vec<u8> = word.iter().map(|&x| x + 97).collect();
    String::from_utf8(utf).expect(&*format!("Failed at line {}", line!()))
}

//Maybe change it to return Option<Word>?
///Converts "abc"-string to `Word`.
///#Examples
///```
///assert_eq!(string_to_word("abc"), vec![0u8, 1, 2]);
///assert_eq!(string_to_word(""), vec![]);
///```
pub fn string_to_word(in_str: &str) -> Word {
    in_str.as_bytes().iter().map(|x| x - 97).collect()
}

///Returns `Vec` which contains all prefixes of the word (word itself included).
///#Examples
///```
///let word = vec![0u8, 1, 2, 0];
///let expected = vec![vec![0u8], vec![0u8, 1], vec![0u8, 1, 2]];
///assert_eq!(generate_backtrack(word), expected);
///```
pub fn generate_backtrack(word: &Word) -> Vec<Word> {
    let mut result = Vec::with_capacity(word.len() - 1);
    for i in (1..word.len()).rev() {
        result.push(word[0..i].to_vec());
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn spawn_wrapper_children() {
        let ww = WordWrapper::new(vec![0u8, 1, 0]);
        let expected_children = vec![
            WordWrapper::new(vec![0u8, 1, 0, 0]),
            WordWrapper::new(vec![0u8, 1, 0, 1]),
            WordWrapper::new(vec![0u8, 1, 0, 2])
        ];
        let children = ww.spawn_children();
        assert_eq!(children, expected_children);
    }

    #[test]
    fn gen_new() {
        let words = vec!["abc".to_owned(), "aabb".to_owned(), "abba".to_owned()];
        let aa2f_words: Vec<String> = words.into_iter()
            .filter(|x| is_aa2f_full_check(&string_to_word(x))).collect();
        assert_eq!(aa2f_words, vec!["abc", "aabb"]);
        let mut gen = Generator::new(&aa2f_words, false, 1);
    }

    #[test]
    fn gen_until() {
        let mut gen = Generator::new(&vec!["a".to_owned()], false, 1);
        let result = gen.generate_until(|x| x.len() > 4);
        assert_eq!(result.len(), 5);
    }

    #[test]
    fn test_parikh() {
        assert_eq!(calculate_parikh_vector(&vec![0u8, 1, 0, 2, 1]), [2, 2, 1]);
        assert_eq!(calculate_parikh_vector(&vec![]), [0,0,0]);
    }

    #[test]
    fn test_aa2f_partial() {
        assert_eq!(is_aa2f_partial_check(&vec![0u8, 1, 0, 2, 0, 1, 0]), true);
        assert_eq!(is_aa2f_partial_check(&vec![0u8, 1, 0, 1]), false);
        assert_eq!(is_aa2f_partial_check(&vec![0u8, 1, 0, 1, 2]), true);
    }

    #[test]
    fn test_aa2f_full() {
        assert_eq!(is_aa2f_full_check(&vec![0u8, 1, 0, 2, 0, 1, 0]), true);
        assert_eq!(is_aa2f_full_check(&vec![0u8, 1, 0, 1]), false);
        assert_eq!(is_aa2f_full_check(&vec![0u8, 1, 0, 1, 2]), false);
    }

    #[test]
    fn test_w2s() {
        assert_eq!(word_to_string(&vec![0u8, 1, 2]), "abc".to_owned());
        assert_eq!(word_to_string(&vec![]), "".to_owned());
    }

    #[test]
    fn test_s2w() {
        assert_eq!(string_to_word("abc"), vec![0u8, 1, 2]);
        assert_eq!(string_to_word(""), vec![]);
    }

    #[test]
    fn test_backtrack() {
        let word = vec![0u8, 1, 2, 0];
        let expected = vec![vec![0u8, 1, 2], vec![0u8, 1], vec![0u8]];
        assert_eq!(generate_backtrack(&word), expected);
    }
}
