## AA2F Generator

### About

The, hopefully, final attempt at improving performance of [AA2F-Generator](../../../aa2f-generator). Mostly has the same structure as [JS version](../../../aa2f-node). My [B.Eng. thesis](../../../thesis) mostly describes implementation process of this program.

### Installation
1. Install [git](https://git-scm.com/).
2. Install latest stable 1.x.y version of [Rust](https://www.rust-lang.org/).
3. Clone the repository.

        git clone https://bitbucket.org/AlGvrl/aa2f-rs.git

4. Compile the code.

        cargo build
        cargo build --release

5. Run the executable in the `target/release` directory. Help is available via `-h` flag.

To compile and view the documentation run `cargo doc` and open the `target/doc/aa2f_rs/index.html` file.

To run tests use `cargo test`. Contact me if anything fails.

### Branches

* `vecs` -- main branch of the repository. The code in this branch is focused simply on generating the longest possible word.
* `extensions` -- checks if words in given file can be extended both to the left and to the right by given amount of characters or not.
* `DLTree` -- experimental data structure to hold existing words. Proved unsuccessful.
* `sandelius` -- generates words randomly and checks if they are aa2f. Proved unsuccessful.